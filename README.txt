name:    plazes module
license: GPL
by:      Breyten Ernsting (bje@dds.nl)
requirements: Drupal 4.6.3 or above.

1. Copy the files to a directory of your choice (say, site/modules/plazes)
2. Activate the module in admin/modules
3. Go the settings page for this module: admin/settings/plazes
4. adjust the settings.
5. optionally install GMap filter module: http://drupal.org/node/33591
6. enjoy!
